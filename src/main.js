import Vue from 'vue'
import App from './App.vue'
import router from './router'

import '../src/plugins/vant'

import  axios from 'axios'
Vue.prototype.$axios = axios
// axios 请求拦截器
axios.interceptors.request.use(config =>{
  if (localStorage.getItem('userInfo')) {
    config.headers.Authorization = JSON.parse(localStorage.getItem('userInfo')).token;
  }
  // 必须进行return config
  return config;
})

axios.interceptors.response.use(config =>{
  // 必须进行return config
  return config;
})

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
