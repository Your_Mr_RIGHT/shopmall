import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ShoppingMall from '../components/pages/ShoppingMall.vue'
import Register from '../components/pages/Register.vue'
import Login from '../components/pages/Login.vue'
import Goods from '../components/pages/Goods.vue'
import Category from '../components/pages/Category.vue'
import Cart from '../components/pages/Cart.vue'
import Main from '../components/pages/Main.vue'
import User from '../components/pages/User.vue'

Vue.use(VueRouter)

const routes = [
  {path:'/',name:'Main',component:Main,
    children:[
      { path: '/', name: 'ShoppingMall', component: ShoppingMall},
      { path:'/category',name:'Category',component:Category},
      { path:'/cart',name:'Cart',component:Cart},
      { path:'/user',name:'User',component:User},
    ]  
  },
 
  { path:'/register',name:'Register',component:Register},
  { path:'/login',name:'Login',component:Login},
  { path:'/goods',name:'Goods',component:Goods},
 
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  //  to 将要访问的路径
  //  from 代表从那个路径跳转而来
  //  next 是一个函数 代表方形
  //  next()  放行   next('/login') 强制跳转至login
  
  if (to.path === "/login") return next();
    //  判断本地是否存在用户信息、token
    if (!localStorage.getItem("userInfo")) {
    return next("/login");
    }
   next();
  });


export default router
