const BASEURL = 'https://console-mock.apipost.cn/app/mock/project/98977122-4758-48a9-ca48-9b57693df37c/'
const LOCALURL = "http://127.0.0.1:7001"
const URL = {
  getShoppingMallInfo : BASEURL + 'datalist' ,   //商城首页 获取数据接口
  register: LOCALURL+'/adminUser/add', //注册接口
  login: LOCALURL+'/adminUser/login', //登录接口
  goodDetails: LOCALURL+'/goodDetail', //商品详情接口
  category: LOCALURL+'/category', //商品大类接口
  categorySub: LOCALURL+'/category_sub', //商品小类接口
  goodlist: LOCALURL+'/getGoodsListByCategorySubID', //商品小类获取商品数据
}

module.exports = URL